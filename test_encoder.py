
# This code has been substantially modified from the code available in github repositry of 3D StyleGAN for medical images (https://github.com/sh4174/3DStyleGAN)
# ------------------------------------------------------------------
# Encoder for 3D StyleGAN and Latent Space Statistics/Embedding
# ------------------------------------------------------------------
# Original StyleGAN2 Copyright and Licences
# ------------------------------------------------------------------
# Copyright (c) 2019, NVIDIA Corporation. All rights reserved.
#
# This work is made available under the Nvidia Source Code License-NC.
# To view a copy of this license, visit
# https://nvlabs.github.io/stylegan2/license.html

import argparse
import numpy as np
import dnnlib
import dnnlib.tflib as tflib
import re
import sys
import nibabel as nib
import os
import pandas as pd

import encoder
import pretrained_networks
from training import dataset
from training import misc

#----------------------------------------------------------------------------   

def test_encoder_real_images_from_csv(gan_network_pkl, enc_network_pkl, csv_path, num_images, generate_gan_output):
    
    if generate_gan_output:
        print('Loading GAN from "%s"...' % gan_network_pkl)
        _G, _D, Gs = pretrained_networks.load_networks(gan_network_pkl)
           
        Gs_kwargs = dnnlib.EasyDict()
        Gs_kwargs.randomize_noise = True
        Gs_kwargs.truncation_psi = 1 # truncation_psi
        #Gs_kwargs.output_transform = dict(func=tflib.convert_3d_images_to_uint8, nchwd_to_nhwdc=True)
        Gs_kwargs.minibatch_size = 1
    
    print('Loading Encoder from "%s"...' % enc_network_pkl)
    E = pretrained_networks.load_encoder(enc_network_pkl)
    
    
    df1 = pd.read_csv(csv_path)
    
    if generate_gan_output:
        # Randomly shuffle dataframe when generating gan output so that we can check a scans randomly
        df1 = df1.sample(frac=1).reset_index(drop=True)
        
    df1['LATENT'] = 0        
    
    if num_images == -1:
        num_images = len(df1)

    for image_idx in range(num_images):
        print('Encoding image %d/%d ...' % (image_idx, num_images))
        
        # Read the nii image using nib.load()
        img_nib = nib.load(df1['PETSCANAME'][image_idx])
        images = np.asarray( img_nib.get_fdata() )
        images = np.expand_dims(np.expand_dims(images, axis=0), axis=0)
        images = (images/np.max(images)).clip(0, 1.0).astype(np.float32)
        #images = misc.adjust_dynamic_range(images, [0, 1.0], [-1, 1]) #(images shape is (1,1,160,160,96))
        
        dlatent = E.run(images)
        df1['LATENT'][image_idx] = list(dlatent)
        
        if generate_gan_output:
            dlatent = np.expand_dims(dlatent, axis=0)
            dlatent_all = np.tile(dlatent,(1,12,1))
            out_image = Gs.components.synthesis.run(dlatent_all, **Gs_kwargs)
            
            img = nib.Nifti1Image(out_image[0, 0, :, :, :].astype(np.float32), np.eye(4))

            out_name = os.path.basename(df1['PETSCANAME'][image_idx])
            nib.save( img, dnnlib.make_run_dir_path(out_name.replace(".nii.gz", "_output_rand_noise.nii.gz")) )
        
    #saving latent vector
    df1.to_csv(dnnlib.make_run_dir_path('encoded_latents.csv'), index=False)


#----------------------------------------------------------------------------

def _parse_num_range(s):
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return list(range(int(m.group(1)), int(m.group(2))+1))
    vals = s.split(',')
    return [int(x) for x in vals]

#----------------------------------------------------------------------------

_examples = '''examples:


  # Encode real images
  python %(prog)s encode-real-images-from-csv --network=gdrive:networks/stylegan2-car-config-f.pkl --dataset=car --data-dir=~/datasets

'''

#----------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(
        description='''Test StyleGAN3D encoder.

        Run 'python %(prog)s <subcommand> --help' for subcommand help.''',
        epilog=_examples,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    subparsers = parser.add_subparsers(help='Sub-commands', dest='command')

    test_encoder_real_images_from_csv_parser = subparsers.add_parser('test-encoder-real-images-from-csv', help='Encode real images')
    test_encoder_real_images_from_csv_parser.add_argument('--gan-network', help='GAN Network pickle filename', dest='gan_network_pkl', required=True)
    test_encoder_real_images_from_csv_parser.add_argument('--encoder-network', help='Encoder Network pickle filename', dest='enc_network_pkl', required=True)
    test_encoder_real_images_from_csv_parser.add_argument('--csv-path', help='Path to csv', required=True)
    test_encoder_real_images_from_csv_parser.add_argument('--num-images', type=int, help='Number of images to encode (default: %(default)s)', default=1)
    test_encoder_real_images_from_csv_parser.add_argument('--result-dir', help='Root directory for run results (default: %(default)s)', default='results', metavar='DIR')
    test_encoder_real_images_from_csv_parser.add_argument('--generate-gan-output', type=int, help='Enable(1) or disable (0) running gan on encoder output', default=0)
    
    args = parser.parse_args()
    subcmd = args.command
    if subcmd is None:
        print ('Error: missing subcommand.  Re-run with --help for usage.')
        sys.exit(1)

    kwargs = vars(args)
    sc = dnnlib.SubmitConfig()
    sc.num_gpus = 1
    sc.submit_target = dnnlib.SubmitTarget.LOCAL
    sc.local.do_not_copy_source_files = True
    sc.run_dir_root = kwargs.pop('result_dir')
    sc.run_desc = kwargs.pop('command')

    func_name_map = {
        'test-encoder-real-images-from-csv': 'test_encoder.test_encoder_real_images_from_csv',
    }
    dnnlib.submit_run(sc, func_name_map[subcmd], **kwargs)

#----------------------------------------------------------------------------

if __name__ == "__main__":
    main()

#----------------------------------------------------------------------------