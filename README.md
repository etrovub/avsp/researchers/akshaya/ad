## 3DStyleGAN for PET images

![3DStyleGAN](images/3DStyleGAN.png)

## Amyloid feature representation and progression modeling pipeline

Encoder(E) projects each PET image into the latent space of 3DStyleGAN (W). The dimensonality of the latent vector(w) is further reduced using PCA. The velocity field from the participants' projected PET image pairs is used to predict trajectories on the PCA subspace. This is then projected back to the 3D-STyleGAN latent space. Using 3DStyleGAN synthesis network, synthetic PET image evolution of synthetic or real patient is generated.

![Progression modelling](images/Progression_modelling_pipeline.png)

## Latent vector of real images from Encoder

python test_encoder.py test-encoder-real-images-from-csv --gan-network=trained_networks/StyleGAN3D-network-snapshot-000160.pkl --encoder-network=trained_networks/Encoder-037400.pkl --csv-path=csv_files/data.csv --num-images=-1 --result-dir=csv_files/latent --generate-gan-output=0

## Image generation using 3DStyleGAN

python run_generator.py generate-images-from-latent --network=trained_GAN/trainset3-network-snapshot-000160.pkl --N=\$p --T=\$i --NPCA=\$n --result-dir=filename

## Folder description

The models of 3DStyleGAN and Encoder are provided in the folder 'trained_networks'

The CSV files in the 'results_GP' folder contain predicted trajectories in w space for 752 subjects projected in the first 5 PCA modes corresponding to 0, 0.3, 7.5, 15, 22.5, and 30 years of evolution (weights_PCA_new_5_t_0.csv to weights_PCA_new_5_t_5.csv).
