# This code has been substantially modified from the code available in github repositry of 3D StyleGAN for medical images (https://github.com/sh4174/3DStyleGAN)
# ------------------------------------------------------------------------
# Generator Script for 3D StyleGAN
# ------------------------------------------------------------------------
# Original StyleGAN2 Copyright
# ------------------------------------------------------------------------
# Copyright (c) 2019, NVIDIA Corporation. All rights reserved.
#
# This work is made available under the Nvidia Source Code License-NC.
# To view a copy of this license, visit
# https://nvlabs.github.io/stylegan2/license.html

import argparse
import numpy as np
import PIL.Image
import dnnlib
import dnnlib.tflib as tflib
import re
import sys

import os 

import pretrained_networks

import nibabel as nib

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

#----------------------------------------------------------------------------

def generate_images_from_latent(network_pkl, N, T, NPCA):
    print('Loading networks from "%s"...' % network_pkl)
    _G, _D, Gs = pretrained_networks.load_networks(network_pkl)
    # Gs = pretrained_networks.load_generator(network_pkl)
    noise_vars = [var for name, var in Gs.components.synthesis.vars.items() if name.startswith('noise')]

    Gs_syn_kwargs = dnnlib.EasyDict()
    Gs_syn_kwargs.randomize_noise = False
    Gs_syn_kwargs.minibatch_size = 1


    # w = np.random.randn(96)/2
    
    # w = np.loadtxt('res_GP/weights_PCA_new_' + str(NPCA) + '_t_' + str(int(T)) + '.csv', delimiter=',')
    w = np.loadtxt('res_GP_noCP/weights_PCA_new_' + str(NPCA) + '_t_' + str(int(T)) + '.csv', delimiter=',')

    w = w[int(N)]

    w = np.expand_dims(np.expand_dims(w, axis=0), axis=0)
    w = np.tile(w, [1,12,1])
    # w = Gs.components.mapping.run( np.random.randn(1, *Gs.input_shape[1:]) , None )

    images = Gs.components.synthesis.run( w, **Gs_syn_kwargs)
    print(images.shape)
    images = np.asarray(np.transpose(images, [0, 2, 3, 4, 1]),dtype=np.float64)
    
    img = nib.Nifti1Image( images[0, :, :, :, 0 ], np.eye(4))

    nib.save( img, dnnlib.make_run_dir_path('w_tmp.nii.gz') )

#----------------------------------------------------------------------------

def _parse_num_range(s):
    '''Accept either a comma separated list of numbers 'a,b,c' or a range 'a-c' and return as a list of ints.'''

    range_re = re.compile(r'^(\d+)-(\d+)$')
    m = range_re.match(s)
    if m:
        return list(range(int(m.group(1)), int(m.group(2))+1))
    vals = s.split(',')
    return [int(x) for x in vals]

#----------------------------------------------------------------------------

_examples = '''examples:

  # Generate ffhq uncurated images (matches paper Figure 12)
  python %(prog)s generate-images --network=gdrive:networks/stylegan2-ffhq-config-f.pkl --seeds=6600-6625 --truncation-psi=0.5

'''

#----------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(
        description='''StyleGAN2 generator.

Run 'python %(prog)s <subcommand> --help' for subcommand help.''',
        epilog=_examples,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    subparsers = parser.add_subparsers(help='Sub-commands', dest='command')
    
    parser_generate_images_from_latent = subparsers.add_parser('generate-images-from-latent', help='Generate images')
    parser_generate_images_from_latent.add_argument('--network', help='Network pickle filename', dest='network_pkl', required=True)
    #parser_generate_images_from_latent.add_argument('--seeds', type=_parse_num_range, help='List of random seeds', required=True)
    parser_generate_images_from_latent.add_argument('--truncation-psi', type=float, help='Truncation psi (default: %(default)s)', default=0.5)
    parser_generate_images_from_latent.add_argument('--result-dir', help='Root directory for run results (default: %(default)s)', default='results', metavar='DIR')
    parser_generate_images_from_latent.add_argument('--N', help='Which patient (default: %(default)s)', default=0)
    parser_generate_images_from_latent.add_argument('--T', help='Which time (default: %(default)s)', default=0)
    parser_generate_images_from_latent.add_argument('--NPCA', help='Which time (default: %(default)s)', default=2)

    args = parser.parse_args()
    kwargs = vars(args)
    subcmd = kwargs.pop('command')

    if subcmd is None:
        print ('Error: missing subcommand.  Re-run with --help for usage.')
        sys.exit(1)

    sc = dnnlib.SubmitConfig()
    sc.num_gpus = 1
    sc.submit_target = dnnlib.SubmitTarget.LOCAL
    sc.local.do_not_copy_source_files = True
    sc.run_dir_root = kwargs.pop('result_dir')
    sc.run_desc = subcmd

    func_name_map = {
        'generate-images-from-latent': 'run_generator.generate_images_from_latent',
    }
    dnnlib.submit_run(sc, func_name_map[subcmd], **kwargs)

#----------------------------------------------------------------------------

if __name__ == "__main__":
    main()

#----------------------------------------------------------------------------
